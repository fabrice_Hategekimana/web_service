# Exercice 1:
Le but est de se familiariser avec les services REST API et le moyen de les interroger.

Il vous suffira de lancer le serveur une seule fois avec la commande `node serveur.js`.

Pour chaque exercice, il vous faudra faire:

Un *GET* request à l'adresse http:localhost:3000/
Un *POST* request à l'adresse http:localhost:3000/post
Un *PUT* request à l'adresse http:localhost:3000/put
Un *DELETE* request à l'adresse http:localhost:3000/delete

## 1. Curl
Nous allons premièrement utiliser curl comme premier client.
Curl est un outil par ligne de commande.
Vous pouver l'installer depuis [ici](https://curl.se/windows/).
Vous trouverez de la documentation utile dans ce [lien](https://linuxize.com/post/curl-rest-api/)

## 2. Postman
Postman est un client GUI spécialisé.
Vous pouver l'installer depuis [ici](https://app.getpostman.com/app/download/win64)
Vous trouverez de la documentation utile dans ce [lien](https://www.guru99.com/postman-tutorial.html).

## Vscode REST client
Vscode a une extention assez pratique pour faire des requêtes sur un serveur REST API.
Vous pouvez l'installer dans le gestionnaire d'extention de Vscode.
Vous trouverez de la documentation sur la description de l'extention ou sur ce [lien](https://marketplace.visualstudio.com/items?itemName=humao.rest-client)
