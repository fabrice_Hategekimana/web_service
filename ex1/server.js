const express = require("express");
const app = express();

app.listen(3000, () => { 
	console.log("Example app listening on port 3000!");
})

app.get("/", (req, res) => { 
	res.send("Welcome to learn backend with express!");

});

app.post("/post", (req, res) => { 
	return res.send({"type": "POST"});
});

app.put("/put", (req, res) => { 
	return res.send({"type": "PUT"});
});

app.delete("/delete", (req, res) => { 
	return res.send({"type": "DELETE"});
});
