from flask import Flask
import time
import threading
app = Flask(__name__)

X = 0

def helloWorld(a):
    return "Hello World!"

@app.route("/", methods=['GET'])
def hello():
    return threading.Thread(target=helloWorld, args=(1,))

@app.route("/value", methods=['GET'])
def getValue():
    global X
    return str(X)

@app.route("/value", methods=['POST'])
def incrementValue():
    global X
    X += 1
    return f"newvalue of X: {X}"


if __name__ == "__main__":
    app.run()
